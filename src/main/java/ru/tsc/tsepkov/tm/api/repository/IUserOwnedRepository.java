package ru.tsc.tsepkov.tm.api.repository;

import ru.tsc.tsepkov.tm.model.AbstractUserOwnedModel;
import java.util.Comparator;
import java.util.List;

public interface IUserOwnedRepository<M extends AbstractUserOwnedModel> extends IRepository<M> {

    M add(String userId, M model);

    List<M> findAll(String userId);

    List<M> findAll(String userId, Comparator<M> comparator);

    M findOneById(String userId, String id);

    M findOneByIndex(String userId, Integer index);

    M remove(String userId, M model);

    M removeById(String userId, String id);

    M removeByIndex(String userId, Integer index);

    boolean existsById(String userId, String id);

    void clear(String userId);

    int getSize(String userId);

}
