package ru.tsc.tsepkov.tm.api.service;

import ru.tsc.tsepkov.tm.enumerated.Role;
import ru.tsc.tsepkov.tm.model.User;

public interface IAuthService {

    User registry(String login, String password, String email);

    void login(String login, String password);

    void logout();

    boolean isAuth();

    String getUserId();

    User getUser();

    void checkRoles(Role[] roles);
}
