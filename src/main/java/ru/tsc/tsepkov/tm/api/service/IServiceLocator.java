package ru.tsc.tsepkov.tm.api.service;

public interface IServiceLocator {

    ICommandService getCommandService();

    IProjectService getProjectService();

    ITaskService getTaskService();

    IProjectTaskService getProjectTaskService();

    ILoggerService getLoggerService();

    IUserService getUserService();

    IAuthService getAuthService();

}
