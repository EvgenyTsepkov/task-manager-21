package ru.tsc.tsepkov.tm.api.service;

import ru.tsc.tsepkov.tm.api.repository.IUserOwnedRepository;
import ru.tsc.tsepkov.tm.enumerated.Sort;
import ru.tsc.tsepkov.tm.model.AbstractUserOwnedModel;
import java.util.List;

public interface IUserOwnedService<M extends AbstractUserOwnedModel> extends IUserOwnedRepository<M>, IService<M> {

    List<M> findAll(String userId, Sort sort);

}
