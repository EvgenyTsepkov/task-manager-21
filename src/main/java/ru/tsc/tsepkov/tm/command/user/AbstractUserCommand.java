package ru.tsc.tsepkov.tm.command.user;

import ru.tsc.tsepkov.tm.api.service.IUserService;
import ru.tsc.tsepkov.tm.command.AbstractCommand;

public abstract class AbstractUserCommand extends AbstractCommand {

    protected IUserService getUserService() {
        return serviceLocator.getUserService();
    }

    @Override
    public String getArgument() {
        return null;
    }

}
